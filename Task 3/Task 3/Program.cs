﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {

            int Menu;
            string Pizzatopping;
            int o;
            int m;
            var type = new List<string> { "Meat Lovers", "Lovers of meat", "Non vegatarian", "Beef,chicken,Lamb and pork pizza" };
            var orderdetails = new List<Tuple<string, string, double>>();
            double cost = 0;
            string balance;
            double Pay = 0;


            List<Tuple<string, double>> Pizza = new List<Tuple<string, double>>();
            Pizza.Add(Tuple.Create("Small", 18.99));
            Pizza.Add(Tuple.Create("Medium", 49.99));
            Pizza.Add(Tuple.Create("Monster", 200.00));

            List<Tuple<string, double>> Drink = new List<Tuple<string, double>>();
            Drink.Add(Tuple.Create("Dragons Blood", 2.99));
            Drink.Add(Tuple.Create("Nectar of the gods", 2.99));
            Drink.Add(Tuple.Create("Souls of the Fallen", 2.99));
            Drink.Add(Tuple.Create("Water...", 800.00));


            Customerdetails:

            Console.WriteLine("Enter at your own peril only the hungry may survive");
            Console.WriteLine(" press any key to continue");
            Console.ReadLine();
            Customer.Details();


        Menu:


            Console.Clear();
            customerOrder.display();
            Console.WriteLine();
            Console.WriteLine("Please select one of the following");
            Console.WriteLine("1. Pizza");
            Console.WriteLine("2. Drinks");
            Console.WriteLine("3. Place Order");
            Console.WriteLine();


            if (int.TryParse(Console.ReadLine(), out m))
            {
                switch (m)
                {
                    case 1:

                    Pizzamenustart:
                        Console.Clear();
                        Console.WriteLine();
                        Console.WriteLine("Current Order:");
                        customerOrder.display();
                        for (o = 0; o < type.Count; o++)
                        {
                            Console.WriteLine($"{o + 1}. {type[o]}");
                        }
                        Console.WriteLine();
                        Console.WriteLine($"{o + 1}. Return to menu");

                        if (int.TryParse(Console.ReadLine(), out m))
                        {
                            if (m == o + 1)
                            {
                                goto Menu;
                            }
                            else if (m > o + 1)
                            {
                                
                                Console.WriteLine();
                                Console.WriteLine("Invalid selection.");
                                Console.ReadLine();
                                goto Pizzamenustart;
                            }
                            else
                            {
                                Pizzatopping = type[m- 1];
                                Console.Clear();
                                customerOrder.display();
                                for (o = 0; o < Pizza.Count; o++)
                                {
                                    Console.WriteLine($"{o + 1}. {Pizza[o].Item1}");
                                }
                                Console.WriteLine();
                                Console.WriteLine($"{o + 1}. Return to menu");

                                if (int.TryParse(Console.ReadLine(), out m))
                                {
                                    if (m == o + 1)
                                    {
                                        goto Menu;
                                    }
                                    else if (m > o + 1)
                                    {
                                        Console.WriteLine();
                                        Console.WriteLine("Invalid selection.");
                                        Console.ReadLine();
                                        goto Pizzamenustart;
                                    }
                                    else
                                    {
                                        orderdetails.Add(Tuple.Create(Pizzatopping, Pizza[m - 1].Item1, Pizza[m - 1].Item2));
                                        customerOrder.order.Add(Tuple.Create(Pizzatopping, Pizza[m - 1].Item1, Pizza[m - 1].Item2));
                                    }
                                    goto Pizzamenustart;
                                }
                                else
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Invalid selection.");
                                    Console.ReadLine();
                                    goto Pizzamenustart;
                                }
                            }

                        }
                        else
                        {
                            Console.WriteLine();
                            Console.WriteLine("Invalid selection.");
                            Console.ReadLine();
                            goto Pizzamenustart;
                        }

                    case 2:
                    Drinkmenustart:


                        Console.Clear();

                        customerOrder.display();
                        for (o = 0; o < Drink.Count; o++)
                        {
                            Console.WriteLine($"{o + 1}. {Drink[o].Item1}");
                        }
                        Console.WriteLine();
                        Console.WriteLine($"{o + 1}. Return to menu");


                        if (int.TryParse(Console.ReadLine(), out m))
                        {
                            if (m == o + 1)
                            {
                                goto Menu;
                            }
                            else if (m > o + 1)
                            {
                                Console.WriteLine();
                                Console.WriteLine("Invalid selection.");
                                Console.ReadLine();
                                goto Drinkmenustart;
                            }
                            else
                            {
                                orderdetails.Add(Tuple.Create("Drink     ", Drink[m - 1].Item1, Drink[m - 1].Item2));
                                customerOrder.order.Add(Tuple.Create("Drink     ", Drink[m - 1].Item1, Drink[m - 1].Item2));
                            }
                            goto Drinkmenustart;
                        }

                        else
                        {
                            Console.WriteLine();
                            Console.WriteLine("Invalid selection");
                            Console.ReadLine();
                            goto Drinkmenustart;
                        }
                    case 3:
                    orderfinal:


                        Console.Clear();
                        customerOrder.display();
                        Console.WriteLine("Do you dare to order more?");
                        Console.WriteLine("1. Never");
                        Console.WriteLine("2. Always");

                        if (int.TryParse(Console.ReadLine(), out m))
                        {
                            switch (m)
                            {
                                case 1:
                                    Console.Clear();
                                    for (o = 0; o < orderdetails.Count; o++)
                                    {
                                        balance = String.Format("{0:C}", orderdetails[o].Item3);
                                        Console.WriteLine($"{orderdetails[o].Item1}      {orderdetails[o].Item2}      {balance}");
                                        cost = cost + orderdetails[o].Item3;
                                    }
                                    balance = String.Format("{0:C}", cost);
                                    Console.WriteLine();
                                    Console.WriteLine($"cost:   ${cost}");
                                    Console.WriteLine();

                                    Console.WriteLine(" How much cash have you got?");
                                    Pay = double.Parse(Console.ReadLine());

                                    if (Pay > cost)
                                    {
                                        Pay = Pay- cost;
                                        Console.WriteLine($"Thanks. here is your change ${Pay}");
                                        Console.ReadLine();
                                    }
                                    else if (Pay == cost)
                                    {
                                        Console.WriteLine("Thank you");
                                        Console.ReadLine();
                                    }
                                    else if (Pay < cost)
                                    {
                                        cost = cost - Pay;
                                        Console.WriteLine("You dont have enough to pay for the order");
                                        Console.ReadLine();
                                    }
                                    Environment.Exit(0);
                                    break;

                                case 2:
                                    goto Menu;
                            }
                        }
                        break;

                    default:

                        
                        Console.WriteLine("Invalid selection.");
                        Console.ReadLine();
                        goto Menu;




                }
            }
            else
            {
              
                Console.WriteLine("Invalid selection.");
                Console.ReadLine();
                goto Menu;
            }
            customerOrder.display();
        }

    }
  }




public static class customerOrder
{

    public static List<Tuple<string, string, double>> order = new List<Tuple<string, string, double>>();

    public static void display()
    {
        int i;
        double cost = 0;
        string balance;


        Console.WriteLine($"Name: {Customer.Name}");
        Console.WriteLine($"Address: {Customer.Address}");
        Console.WriteLine($"Contact Number {Customer.contactNumber}");

        Console.WriteLine("Order Summary:");

        for (i = 0; i < order.Count; i++)
        {
            balance = String.Format("{0:C}", order[i].Item3);
            Console.WriteLine($"{order[i].Item1}      {order[i].Item2}      {balance}");
            cost = cost + order[i].Item3;
        }
        balance = String.Format("{0:C}", cost);
        Console.WriteLine();
        Console.WriteLine($"     cost:        ${cost}");
    }

}

public static class Customer
            {
                public static string Name;
                public static string Address;
                public static int contactNumber;

                public static void Details()
                {
                    int m;

                Start:

                    Console.Clear();

                    Console.WriteLine();
                    Console.WriteLine("What is the name for the order?");
                    Console.WriteLine();

                    Name = Console.ReadLine();

                    Console.WriteLine();
                    Console.WriteLine("And an address for delivery.");
                    Console.WriteLine();

                    Address = Console.ReadLine();

                    Console.WriteLine();
                    Console.WriteLine("What is your contact number");
                    Console.WriteLine();

                    contactNumber = int.Parse(Console.ReadLine());

                }
    }

